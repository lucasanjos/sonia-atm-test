# SONIA.AI ATM TEST


## Install the project

Simply clone the repository and run `npm i` to install the dependencies.

## Database

This project is using **MongoDB**, in order to run the project, you need to run it in the local machine. 
Regards about **MongoDB**: Since this is a test project, i'm using MongoDB, but if it was a real ATM or money based project, i would not use it, 'cause MongoDB doesn't guarantee ACID. 

## Run the Project

Run the MongoDB and then `npm run dev` in the project path.
But the the project is already online up and running! 
The URL is already in the **test** file, so you can test without the need of installing and setting up the project (:

## About the Project and System Architecture

I'm not using any backend web frameworks, using the NodeJS RequestHandler to deal with the requests. I'm also using the native es7 async/await. The system structure is separated in the folders Models (for the MongoDB Models) / Repository (has all the database logics) / Services (has all the business logics).
For Authentication, i'm using JWT Token that expires in 3 minutes (common ATM time use) and the User's PIN is encrypted in the database.

## Test

I'm using the Axios framework to make the requests to the NodeJS Server and logging the responses. Just run `npm test` to perform the tests.

## Requests

### /auth
[POST] 
{ "cpf": 12345678, "pin": 1234 }
This request will authorize the User in the ATM, first checking the CPF, and then, if exists, check the pin, and return the JWT Token.
If the user tries to auth with the wrong PIN more than 3 times, the API blocks the user.

### /withdraw
[POST]
{ "token": token, "amount": 5 }
This request will perform a withdraw in the User's account.
If the amount that the user is trying to withdraw is more than he has in the account, the API returns a error message to the user.

### /deposit

[POST]
{ "token": token, "amount": 5, "toAccount": 123456 }
This request will perform a deposit, the API first check if the destination account exists, and then perform the deposit.

### /transactions

[POST]
{ "token": token } or  { "token": token, "beginDate": isoDate, "endDate": isoDate }
This request will perform a list of all the user's transactions or filter the transactions by date, return an array of objects.

## Author

Lucas Anjos, passionate about development and changing old paradigms (:

lucas.cavalcante.anjos@gmail.com
