const http = require('http')
const port = 5000
const mongoose = require('mongoose')

const services = require('./services/index') //importing all services at once

mongoose.connect('mongodb://localhost/atm')

const requestHandler = async (request, response) => {

  const body = await getBody(request)

  if (request.url === '/auth')
    response.end(await services.Auth.authorize(body))

  if (request.url === '/deposit')
    response.end(await services.Deposit.receive(body))

  if (request.url === '/withdraw')
    response.end(await services.Withdraw.remove(body))

  if (request.url === '/transactions')
    response.end(await services.Transactions.list(body))

  response.end('ATM Service (:')

}

const server = http.createServer(requestHandler)

server.listen(port, (err) => {
  if (err)
    return console.log('Error on server: ', err)

  console.log(`Server listening on ${port}`)
})

function getBody(request) {
  return new Promise((resolve, reject) => {
    let body = ''
    request.on('data', chunk => {
      body += chunk.toString();
    })
    request.on('end', () => {
      resolve(JSON.parse(body))
    })
  })
}