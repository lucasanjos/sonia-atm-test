const mongoose = require('mongoose')

const Account = new mongoose.Schema({
    created: { type: Date, required: true, default: Date.now },
    number: { type: Number, required: true },
    amount: { type: Number, required: true, default: 0 }
})

module.exports = mongoose.model('account', Account)
