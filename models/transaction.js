const mongoose = require('mongoose')

const Transaction = new mongoose.Schema({
    date: { type: Date, required: true, default: Date.now },
    type: { type: String, required: true }, // withdraw or deposit
    amount: { type: Number, required: true },
    account: { type: mongoose.Schema.Types.ObjectId, ref: 'account', required: true },
    toAccount: { type: mongoose.Schema.Types.ObjectId, ref: 'account', required: true },
})

module.exports = mongoose.model('transaction', Transaction)
