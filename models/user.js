const mongoose = require('mongoose')
const bcrypt = require('bcrypt')

const User = new mongoose.Schema({
    created: { type: Date, required: true, default: Date.now },
    cpf: { type: String, required: true },
    pin: { type: String, required: true }, // String so the bcrypt can encrypt the pin
    account: { type: mongoose.Schema.Types.ObjectId, ref: 'account', required: true },
    tries: { type: Number, required: true, default: 0 }
})

User.pre('save', function preSave (next) {
  const user = this

  new Promise((resolve, reject) => {
    bcrypt.genSalt(10, (err, salt) => {
      if (err) { return reject(err) }
      resolve(salt)
    })
  })
  .then(salt => {
    bcrypt.hash(user.pin, salt, (err, hash) => {
      if (err) throw new Error(err)
      user.pin = hash
      next(null)
    })
  })
  .catch(err => next(err))
})

User.methods.validatePin = function validatePin (pin) {
  const user = this
  return new Promise((resolve, reject) => {
    bcrypt.compare(pin, user.pin, (err, isMatch) => {
      if (err) 
        return reject(err)
      resolve(isMatch)
    })
  })
}

module.exports = mongoose.model('user', User)
