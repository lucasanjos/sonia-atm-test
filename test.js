const axios = require('axios')

// const url = 'http://localhost:5000' // local
const url = 'http://159.203.189.48:5000' // prod

// consts to use in the tests:
const cpf = '39402644899'
const pin = '1234'
const amount = 5
const toAccount = 384795
const beginDate = '2018-03-23'
const endDate = '2018-03-25'


test()

async function test() {

    console.log(`
    ************ URL ************
    ${url}
    *****************************           
    `)

    const token = await performAuthUser() // auth user and get token for next requests
    const withdraw = await performWithdraw(token) // perform a withdraw
    const deposit = await performDeposit(token) // perform a deposit
    const listTransactions = await performListTransactions(token) // perform a list transactions
    const listTransactionsByDate = await performListTransactionsByDate(token) // perform a list transactions by date

}

async function performAuthUser() {

    try {

        const response = await axios.post(`${url}/auth`, {
            cpf,
            pin
        })

        console.log(`
        ************ TOKEN ************
        ${response.data}
        *******************************
        `)
        return response.data

    } catch (error) {
        console.error(error);
    }
}

async function performWithdraw(token) {

    try {
        const response = await axios.post(`${url}/withdraw`, {
            token,
            amount
        })

        console.log(`
        ************ $${amount} WITHDRAW *******************
        ${response.data}
        ********************************************
        `)

    } catch (error) {
        console.error(error);
    }

}

async function performDeposit(token) {

    try {
        const response = await axios.post(`${url}/deposit`, {
            token,
            amount,
            toAccount
        })

        console.log(`
        ************ $${amount} DEPOSIT ******************
        ${response.data}
        ********************************************
        `)

    } catch (error) {
        console.error(error);
    }

}

async function performListTransactions(token) {

    try {
        const response = await axios.post(`${url}/transactions`, {
            token,
        })

        console.log(`
        ************ LIST TRANSACTIONS ******************
        ${JSON.stringify(response.data)}
        ********************************************
        `)

    } catch (error) {
        console.error(error);
    }

}

async function performListTransactionsByDate(token) {

    try {
        const response = await axios.post(`${url}/transactions`, {
            token,
            beginDate,
            endDate
        })

        console.log(`
        ************ LIST TRANSACTIONS BY DATE FILTER ******************
        ${JSON.stringify(response.data)}
        ******************************************************************
        `)

    } catch (error) {
        console.error(error);
    }

}