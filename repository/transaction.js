const Transaction = require('../models/transaction')

class TransactionRepository {

    async create(type, amount, account, toAccount) {
        const transac = new Transaction({
            type,
            amount,
            account,
            toAccount
        })
        return await transac.save()
    }

    async getAll(account) {
        return await Transaction.find({
            account
        }).populate('account').populate('toAccount').lean()
    }

    async getAllByDate(account, beginDate, endDate) {
        return await Transaction.find({
            account,
            date: {
                $gte: beginDate,
                $lte: `${endDate}"23:59:59.999Z`
            }
        }).populate('account').populate('toAccount').lean()
    }

}

module.exports = new TransactionRepository()