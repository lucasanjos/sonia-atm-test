const Account = require('../models/account')

class AccountRepository {

    async findByNumber(number) {
        return Account.findOne({ number })
    }

    async removeMoney(id, total, amount) {
        return Account.findByIdAndUpdate(id, {
            amount: total - amount
        })
    }

    async receiveMoney(id, total, amount) {
        return Account.findByIdAndUpdate(id, {
            amount: total + amount
        })
    }

}

module.exports = new AccountRepository()