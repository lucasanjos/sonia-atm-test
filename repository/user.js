const User = require('../models/user')

class UserRepository {

    async findByCpf(cpf) {
        return await User.findOne({ cpf }).populate('account')
    }

    async addTry(id, tries) {
        return await User.findByIdAndUpdate(id, {
            tries: tries + 1
        })
    }

    async clearTries(id) {
        return await User.findByIdAndUpdate(id, {
            tries: 0
        })
    }

}

module.exports = new UserRepository()