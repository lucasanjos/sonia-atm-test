const Auth = require('./auth')
const TransactionRepository = require('../repository/transaction')

class Transaction {

    async create(type, amount, account, toAccount) {
        await TransactionRepository.create(type, amount, account, toAccount)
    }

    async list(data) {

        const token = data.token
        let transactions

        const user = await Auth.check(token)
        if (!user)
            return 'Token is not valid!'

        const account = user.account

        if (!data.beginDate && !data.endDate)
            transactions = await TransactionRepository.getAll(account._id)

        if (data.beginDate && data.endDate)
            transactions = await TransactionRepository.getAllByDate(account._id, data.beginDate, data.endDate)

        transactions.map(tran => {
            tran.accountNumber = tran.account.number
            delete tran.account
            tran.toAccountNumber = tran.toAccount.number
            delete tran.toAccount
        })

        return JSON.stringify(transactions)
    }

}

module.exports = new Transaction()