const Auth = require('./auth')
const AccountRepository = require('../repository/account')
const Transaction = require('../services/transactions')

class Deposit {

    async receive(data) {

        const token = data.token
        const amount = data.amount
        let toAccount = data.toAccount

        const user = await Auth.check(token)
        if (!user)
            return 'Token is not valid!'

        const account = user.account
        toAccount = await AccountRepository.findByNumber(toAccount)

        if (!toAccount)
            return `The given destination Account doesn't exist!`

        await AccountRepository.receiveMoney(toAccount._id, toAccount.amount, amount)

        Transaction.create('deposit', amount, account._id, toAccount._id)

        return `Deposit successul!`

    }

}

module.exports = new Deposit()