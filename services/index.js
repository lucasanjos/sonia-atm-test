const Auth = require('./auth')
const Withdraw = require('./withdraw')
const Deposit = require('./deposit')
const Transactions = require('./transactions')

module.exports = {
    Auth,
    Withdraw,
    Deposit,
    Transactions
}