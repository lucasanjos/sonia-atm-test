const Auth = require('./auth')
const AccountRepository = require('../repository/account')
const TransactionRepository = require('../repository/transaction')

class Withdraw {

    async remove(data) {

        const token = data.token
        const amount = data.amount

        const user = await Auth.check(token)
        if (!user)
            return 'Token is not valid!'

        const account = user.account

        if (account.amount < amount) // verify if there is the amount in the account
            return `You don't have this amount!`

        await AccountRepository.removeMoney(account._id, account.amount, amount) // remove the money

        TransactionRepository.create('withdraw', amount, account._id, account._id) // create the record on Transactions

        return `Amount left: $${account.amount - amount}`
    }

}

module.exports = new Withdraw()