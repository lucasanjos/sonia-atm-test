const jwt = require('jsonwebtoken')
const User = require('../models/user')
const Account = require('../models/account')
const mongoose = require('mongoose')
const UserRepository = require('../repository/user')

const jwtSecret = 'sonia'

class Auth {

    async authorize(credentials) {

        const cpf = credentials.cpf
        const pin = credentials.pin

        const user = await UserRepository.findByCpf(cpf) // first check if there is this cpf in the database

        if (!user)
            return 'There is no user to this CPF'

        if (user.tries === 3)
            return `There has been 3 unsuccessful tries to this account, so it's blocked!`

        const matchPin = await user.validatePin(pin) // then check if the PIN matches
        if (!matchPin) {
            await UserRepository.addTry(user._id, user.tries) // add unsuccessful try
            return 'Wrong PIN for this CPF'
        }

        await UserRepository.clearTries(user._id) // clear tries after successful auth

        const token = jwt.sign({ // sign token with secret pass expiring in 3 minutes
            data: cpf,
            exp: Math.floor(Date.now() / 1000) + 180
        }, jwtSecret)

        return token

    }

    async check(token) { // check the JWT token and return the user

        try {
            var decoded = jwt.verify(token, jwtSecret);
            return await UserRepository.findByCpf(decoded.data)
        } catch (err) {
            console.log('Verify token error!')
            return false
        }

    }

}

module.exports = new Auth()